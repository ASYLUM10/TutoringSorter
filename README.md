<h1>IT IS HIGHLY ADVISED THAT YOU DO NOT RUN THIS ON A FLASH DRIVE.</h1>

Steps to use.

1: Build release version using visual studios or equivalent.

2: Create new folder where you wish to run the program.

3: Copy the following files from the release folder into the folder you you just created.

    TutoringPracSorter.exe
    SevenZipSharp.dll
    7z.dll
    (One zip file downloaded from RUConnected using the download all submissions feature.)

4: Set up a config file (See Note Bellow)

4: Run the exe.

The result should be that all of the work which was handed in by the students listed
in the main method will be sorted into individual folders in the same folder as the
program was run in.

Note: If you already have a .cfg (config) file for this program simply copy it into 
the run direcory before running the exe otherwise a config file will be generated for you 
at run time.

If a new config file has been added you will need to add the names of your students into 
the config file before the program will work.