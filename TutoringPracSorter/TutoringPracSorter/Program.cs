﻿// Written by Marq Botha
// Date: 22/08/2016
using System.IO;
using SevenZip;
using System;
using System.Collections.Generic;

namespace TutoringPracSorter {
    /// <summary>
    /// Runs in directory where located and extracts practicals of given students to seperate folders.
    /// </summary>
    class Program {
        /// <summary>
        /// Reports an Error and closes the program.
        /// </summary>
        /// <param name="message">Error message</param>
        static void Error(string message) {
            Console.WriteLine(message);
            Console.ReadLine();
            Environment.Exit(0);
        }

        static void Main(string[] args) {
            #region Declarations
            string currDir = Directory.GetCurrentDirectory();
            string tempDir = currDir + "\\temp";
            List<string> names = new List<string>();
            string[] files = Directory.GetFiles(currDir, "*.zip");
            bool keepTemp = false;
            bool keepZip = true;
            #endregion
            #region ConfigFile
            //New Comment
            //Reads in the config file or creates a new one if it does not exist.
            if (File.Exists(currDir + "\\tutoringsorter.cfg")) {
                StreamReader sr = new StreamReader(currDir + "\\tutoringsorter.cfg");
                while (!sr.EndOfStream) {
                    string temp = sr.ReadLine();
                    if (temp.Trim() == "Names:") {
                        temp = sr.ReadLine();
                        do {
                            if (temp.Length >= 1)
                                names.Add(temp.ToLower().Trim());
                            temp = sr.ReadLine();
                        } while (temp.Trim() != "END Names");
                    }
                    if (temp.Trim() == "Settings:") {
                        temp = sr.ReadLine();
                        do {
                            if (temp.Contains("=")) {
                                string[] tempSplit = temp.Split('=');
                                switch (tempSplit[0].Trim()) {
                                    case "keepTemp":
                                        if ((tempSplit[1].ToLower().Trim() != "true") && (tempSplit[1].ToLower().Trim() != "false")) Error("The setting for keepTemp is invalid.");
                                        keepTemp = (tempSplit[1].ToLower().Trim() == "true") ? true : false;
                                        break;
                                    case "keepZip":
                                        if ((tempSplit[1].ToLower().Trim() != "true") && (tempSplit[1].ToLower().Trim() != "false")) Error("The setting for keepZip is invalid.");
                                        keepZip = (tempSplit[1].ToLower().Trim() == "true") ? true : false;
                                        break;
                                    default:
                                        Error("Invalid Setting");
                                        break;
                                }
                            }
                            temp = sr.ReadLine();
                        } while (temp.Trim() != "END Settings");
                    }
                }
                if (names.Count == 0)
                    Error("Your config file either contains no names or is in an incorrect format.");
                sr.Close();
            }
            else {
                using (StreamWriter sw = new StreamWriter(currDir + "\\tutoringsorter.cfg")) {
                    sw.Write("// Insert the names of your students in the names area with one name on each line.\nNames:\n\nEND Names\nSettings:\n//The Temp folder contains the unzipped version of the .zip file.\nkeepTemp=false\nkeepZip=true\nEND Settings");
                    sw.Close();
                }
                Error("No config file found. New Config File was created please enter the names of your students in the config file.");
            }
            #endregion
            #region Error Checking
            //Ensures that there is only one .zip file in the folder where the exe is being run.
            if (files.Length > 1)
                Error("More than one zip file in your folder.");
            //Ensures that there is a .zip fole in the folder where the exe is being run.
            if (files.Length < 1)
                Error("No zip file in folder");
            //Ensures that the 7z.dll is present.
            if (File.Exists(currDir + "\\7z.dll"))
                SevenZipBase.SetLibraryPath(currDir + "\\7z.dll");
            else {
                Error("7z.dll not present");
            }
            //Checks if the temp directory already exists in the current folder.
            if (Directory.Exists(tempDir))
                Error("A folder called temp already exists in the current directory");
            else
                Directory.CreateDirectory(tempDir);
            #endregion

            //Extracts the zip file to a temporary folder.
            using (SevenZipExtractor extractor = new SevenZipExtractor(Path.GetFileName(files[0]))) {
                extractor.ExtractArchive(tempDir);
            }
            if (!keepZip)
                File.Delete(files[0]);
            //Gets contents of the temp folder.
            files = Directory.GetFiles(tempDir);
            foreach (string file in files) {
                {
                    #region declarations
                    //Get filename of the submission with the extension.
                    string filenameWithExt = Path.GetFileName(file);
                    filenameWithExt = filenameWithExt.Substring(filenameWithExt.IndexOf("file") + 5);
                    //Get filename of the submission without the extension.
                    string filename = filenameWithExt.Split('.')[0];
                    //Get name of student who submitted file.
                    string studentName = Path.GetFileName(file).Split('_')[0].ToLower().Trim();
                    //Path to new directory which is created.
                    string newdir = currDir + '\\' + studentName;
                    #endregion
                    //Checks if student is in defined student list.
                    if (names.Contains(studentName)) {
                        //Checks if student directory exists
                        if (!Directory.Exists(newdir))
                            Directory.CreateDirectory(newdir);
                        //Extracts file if common compression format was used and decompress to student's directory.
                        if (file.Contains(".zip") || file.Contains(".rar") || file.Contains(".7z")) {
                            using (SevenZipExtractor extractor = new SevenZipExtractor(file)) {
                                if (!Directory.Exists(newdir + "\\" + filename))
                                    Directory.CreateDirectory(newdir + '\\' + filename);
                                else
                                    Error("Directory already exists for " + Path.GetFileName(newdir + '\\' + filename));
                                extractor.ExtractArchive(newdir + '\\' + filename);
                            }
                        }
                        //If file is not in a common compression format move it to the student's directory.
                        else {
                            if (!keepTemp)
                                File.Move(file, newdir + '\\' + filenameWithExt);
                            else
                                File.Copy(file, newdir + '\\' + filenameWithExt);
                        }
                    }
                    //Delete the file from the temp folder.
                    if (!keepTemp)
                        File.Delete(file);
                }
            }
            //Delete the temp folder.
            if (!keepTemp)
                Directory.Delete(tempDir);
            Console.WriteLine("Completed successfully");
            Console.ReadLine();
        }
    }
}
